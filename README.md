# Introduction
This repo build a cluster with blender, inorder to distribuite render with docker. 

# docker-compose
    $ docker-compose up

# Docker Swarm (untested)
    $ docker swarm init --advertise-addr 192.168....
    $ docker swarm join \
        --token hfjaladafjahfadfkajdsfkja... \
        172.17....
    $ docker stack deploy -c swarm-compose.yml blender_cluster
    $ docker stack services blender_cluster
    $ docker stack rm blender_cluster
    $ docker swarm leave --force

# Intresting reading
https://github.com/elarry/blender-aws/blob/master/blender_gpu_settings.py
